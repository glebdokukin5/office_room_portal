﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound_draw_sphere : MonoBehaviour
{
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.05f);
    }
}
