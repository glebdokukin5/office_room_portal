﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class open_door_script : MonoBehaviour
{
    Animator animator;
    bool doorOpen = false;
    bool PlayerInTrigger = false;
    public AK.Wwise.Event openEvent;
    public AK.Wwise.Event closeEvent;
    public GameObject door;
    void Start()
    {
        animator = GetComponent<Animator>();
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerInTrigger = true;
        }
    }
    void OnTriggerExit(Collider col)
        {
            PlayerInTrigger = false;
        }
    void DoorControl(string direction)
    {
        animator.SetTrigger(direction);
    }
    private void Update()
    {
        if (PlayerInTrigger && !doorOpen && Input.GetKeyDown("e")  )
        {
                doorOpen = true;
                DoorControl("Open");

        }
        else if (PlayerInTrigger && doorOpen && Input.GetKeyDown("e") )
        {
            DoorControl("Close");
                doorOpen = false ;
            }
    }
    void playOpenSound()
    {
        openEvent.Post(door);
    }
    void playCloseSound()
    {
        closeEvent.Post(door);
    }
}
